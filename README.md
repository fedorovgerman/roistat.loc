#Тестовое задание для Roistat

##Задание: 

Создать парсер для access_log файла, вывод должен осуществляться в JSON

##Реализация: 

1. Получение в цикле каждой строки файла access
2. Парсинг с помощью регулярного выражения
    * Особое внимание статусу, после статуса 301 идут доп. символы
    * Иногда не указывается клиент, нужна проверка на пустоту строки
3. Сначала собирается массив значений ($data)
4. Существует две функции, которые формируют версии JSON:
    * Short - считает лишь количество ответов по каждому коду состояния, считает лишь количество посещенных урлов
    * Large - предоставляет более подробную информацию, но также добавляет свойства **count**
    
##Замечания:

Очевидно, что сейчас скрипт работает не оптимально и требуется его доработка с целью повышения производительности и уменьшения нагрузки. Но поставленную задачу решает - предоставляет пользователю развертную информацию по файлу access_log

##Short Output JSON

```json
{
    "views":16,
    "urls":5,
    "statuses":{
        "200":14,
        "301":2
    },
    "traffic":212816,
    "crowlers":{
        "Gecko":2,
        "AppleWebKit":10,
        "Googlebot":2
    }
}
```

##Large Output JSON

```json
{
    "views":16,
    "urls":{
        "0":"\/chat.php",
        "1":"\/mod.php",
        "2":"\/app\/engine\/api.php",
        "3":"\/app\/modules\/randomgallery.php",
        "4":"\/chat.php?id=a65",
        "count":5
    },
    "statuses":{
        "0":{"200":"\/chat.php"},
        "1":{"301":"\/mod.php"},
        "2":{"200":"\/app\/engine\/api.php"},
        "3":{"200":"\/app\/modules\/randomgallery.php"},
        "4":{"200":"\/chat.php?id=a65"},
        "5":{"200":"\/app\/engine\/api.php"},
        "6":{"200":"\/chat.php?id=a65"},
        "7":{"200":"\/app\/modules\/randomgallery.php"},
        "8":{"200":"\/chat.php"},
        "9":{"301":"\/mod.php"},
        "10":{"200":"\/app\/engine\/api.php"},
        "11":{"200":"\/app\/modules\/randomgallery.php"},
        "12":{"200":"\/chat.php?id=a65"},
        "13":{"200":"\/app\/engine\/api.php"},
        "14":{"200":"\/chat.php?id=a65"},
        "15":{"200":"\/app\/modules\/randomgallery.php"},
        "count":{
            "200":14,
            "301":2
        }
    },
    "traffic":212816,
    "crowlers":{
        "Gecko":2,
        "AppleWebKit":10,
        "Googlebot":2
    }
}
```