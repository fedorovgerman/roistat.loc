<?

$descriptor = fopen('access.log', 'r');
$pattern = "/(?'ip'\S+) (\S+) (\S+) \[(?'date'[^:]+):(?'time'\d+:\d+:\d+) ([^\]]+)\] \"(?'method'\S+) (?'url'.*?) (\S+)\" (?'status'\S+|\S+\s-\s\S+) (?'traffic'\S+) (\".*?\") (?'client'\".*?\")/";
$data = [];

if ($descriptor) {
	$data['views'] = 0;
	$data['urls'] = [];
	$data['statuses'] = [];
	$data['traffic'] = 0;
	$data['crowlers'] = [];
    while (($string = fgets($descriptor)) !== false) {
        preg_match ($pattern, $string, $result);

        //Получение уникальных сайтов
    	$data['views']++;
    	if (!in_array($result['url'], $data['urls'])){
    		array_push($data['urls'], $result['url']);
    	}

    	//Получаем все коды состояний
    	preg_match('/\S+/', $result['status'], $status);
	  	array_push($data['statuses'], [$status[0] => $result['url']]);

    	//Получаем суммарный траффик
    	$data['traffic'] += $result['traffic'];

    	//Получаем клиенты, исключаем пустые строки
    	preg_match("/(\S+) (\(.*?)\) (?'client'\w+)/", $result['client'], $client);
    	if ((!array_key_exists($client['client'], $data['crowlers'])) AND $client['client'] !== null){
    		$data['crowlers'] += [$client['client'] => 1];
    	} elseif($client['client'] !== null) {
    		$data['crowlers'][$client['client']]++;
    	}


    }
    fclose($descriptor);

} else {
    echo 'Невозможно открыть указанный файл';
}

function makeShortJson($array){
	$resultArr = [];
	$resultArr['views'] = $array['views'];
	$resultArr['urls'] = count($array['urls']);
	$resultArr['statuses'] = [];

	foreach ($array['statuses'] as $item) {
		foreach ($item as $status => $url) {
			if (!array_key_exists($status, $resultArr['statuses'])){
			$resultArr['statuses'] += [$status => 1];
			} else {
			$resultArr['statuses'][$status]++;
			}
		}
	}

	$resultArr['traffic'] = $array['traffic'];
	$resultArr['crowlers'] = $array['crowlers'];

	return json_encode($resultArr);
}

function makeLargeJson($array){
	$array['urls']['count'] = count($array['urls']);
	$array['statuses']['count'] = [];

	foreach ($array['statuses'] as $item) {
		foreach ($item as $status => $url) {
			if (!array_key_exists($status, $array['statuses']['count'])){
			$array['statuses']['count'] += [$status => 1];
			} else {
			$array['statuses']['count'][$status]++;
			}
		}
	}

	return json_encode($array);
}

echo "<pre>";
echo makeShortJson($data);
